package com.learning.blockchain.singer.reactor;

import com.learning.blockchain.singer.entity.Singer;
import com.learning.blockchain.singer.repository.SingerPO;
import com.learning.blockchain.singer.repository.SingerRepository;
import com.learning.blockchain.singer.repository.SingerRepositoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/singers")
public class SingerController {

    @Autowired
    SingerRepository singerRepository;

    @Autowired
    SingerRepositoryMapper repoMapper;

    @GetMapping
    public Flux<SingerPO> getAllSingers() {
        return singerRepository.findAll();
    }

    @PostMapping
    public Mono<SingerPO> insertSinger(@RequestBody Singer singer) {
        return singerRepository.insert(repoMapper.toPO(singer));
    }
}
