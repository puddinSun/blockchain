package com.learning.blockchain.singer.repository;

import com.learning.blockchain.singer.entity.Singer;
import com.learning.dockercompose.shared.mapper.BaseRepositoryMapper;
import org.springframework.stereotype.Component;

@Component
public class SingerRepositoryMapper implements BaseRepositoryMapper<Singer, SingerPO> {

    @Override
    public SingerPO toPO(Singer dto) {
        return SingerPO.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();
    }

    @Override
    public Singer toDTO(SingerPO po) {
        return Singer.builder()
                .id(po.getId())
                .name(po.getName())
                .build();
    }
}
