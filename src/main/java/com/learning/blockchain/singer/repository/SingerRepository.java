package com.learning.blockchain.singer.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SingerRepository extends ReactiveMongoRepository<SingerPO, String> {

}
