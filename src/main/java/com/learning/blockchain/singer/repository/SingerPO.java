package com.learning.blockchain.singer.repository;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Builder(toBuilder = true)
public class SingerPO {
    private String id;
    private String name;
}
