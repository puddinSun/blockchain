package com.learning.blockchain.live.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class LivePriceController {

    @MessageMapping("/prices")
    @SendTo("/topic/process")
    public String showLivePrices(@Payload String param) throws InterruptedException {
        Thread.sleep(1000);
        return param;
    }
}
