package com.learning.blockchain.chain.service;

import com.learning.blockchain.chain.entity.ChainDTO;
import com.learning.blockchain.chain.repository.ChainPO;
import com.learning.blockchain.chain.repository.ChainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChainService {

    @Autowired
    ChainRepository chainRepository;

    public List<ChainDTO> queryAll() {
       return chainRepository.findAll().stream()
               .map(this::convert)
               .collect(Collectors.toList());
    }

    public void addChain(ChainPO chainPO) {
        chainRepository.save(chainPO);
    }

    private ChainDTO convert(ChainPO chainPO) {
        return ChainDTO.builder()
                .id(chainPO.getId())
                .name(chainPO.getName())
                .build();
    }
}
