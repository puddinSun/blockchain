package com.learning.blockchain.chain.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Builder(toBuilder = true)
@Jacksonized
@Getter
public class ChainDTO {
    private String id;
    private String name;
}
