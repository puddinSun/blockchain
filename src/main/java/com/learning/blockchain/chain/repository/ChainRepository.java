package com.learning.blockchain.chain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChainRepository extends MongoRepository<ChainPO, String> {
}
