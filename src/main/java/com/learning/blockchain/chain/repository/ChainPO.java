package com.learning.blockchain.chain.repository;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("chain")
@Getter
@Builder(toBuilder = true)
public class ChainPO {

    @Id
    private String id;

    @Field("name")
    private String name;

}
