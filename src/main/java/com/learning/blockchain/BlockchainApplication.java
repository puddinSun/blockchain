package com.learning.blockchain;

import com.learning.blockchain.chain.entity.ChainDTO;
import com.learning.blockchain.chain.repository.ChainPO;
import com.learning.blockchain.chain.service.ChainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@SpringBootApplication
@RestController
public class BlockchainApplication {

	@Autowired
	ChainService chainService;

	public static void main(String[] args) {
		SpringApplication.run(BlockchainApplication.class, args);
	}

	@GetMapping("/hello")
	public String sayHello(@RequestParam String name) {
		return "Hello " + name;
	}

	@GetMapping("/chains")
	public ResponseEntity<List<ChainDTO>> queryChains() {
		chainService.addChain(ChainPO.builder().name("chain" + new Date().getTime()).build());
		List<ChainDTO> chainByName = chainService.queryAll();
		return ResponseEntity.ok(chainByName);
	}

}
