package com.learning.blockchain.client;

import com.learning.blockchain.singer.entity.Singer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;

@Order(1)
@Slf4j
@Service
public class SingerClient {

    private final WebClient webClient = WebClient.create("http://localhost:8080");

//    @Override
    public void run(ApplicationArguments args) throws Exception {

        webClient.post().uri("/singers").body(
                        Mono.just(Singer.builder()
                                .name("Maroon 5")
                                .build()), Singer.class)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(StandardCharsets.UTF_8)
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve()
                .bodyToMono(Singer.class)
                .subscribe(e -> log.info("Successfully post object using webClient {}", e.toString()));

        webClient.get()
                .uri("/singers")
                .retrieve()
                .bodyToFlux(Singer.class)
                .subscribe(response -> log.info(String.valueOf(response)));
    }
}
