package com.learning.dockercompose.shared.mapper;

public interface BaseRepositoryMapper<T, R> {
    R toPO(T dto);

    T toDTO(R po);
}
